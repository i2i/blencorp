import './App.css'
import Clients from './Clients'
import ClientDetails from './ClientDetails'
import 'bootstrap/dist/css/bootstrap.min.css'
import classNames from 'classnames'
import './App.css'
import { Route, Switch } from 'react-router-dom'

import {
  ApolloClient,
  ApolloProvider,
  gql,
  InMemoryCache,
} from '@apollo/client'

const client = new ApolloClient({
  uri: 'http://localhost:4000/',
  cache: new InMemoryCache(),
})

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <header className={classNames('App-header', 'shadow')}>
          <h5>GraphQL Example</h5>
        </header>
        <main className="container">
          <Switch>
            <Route exact key="home" path="/" component={Clients} />
            <Route key="details" path="/:id" component={ClientDetails} />
          </Switch>
          {/* <Clients />
          <ClientDetails /> */}
        </main>
      </div>
    </ApolloProvider>
  )
}

export default App
