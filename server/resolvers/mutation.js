module.exports = {
  addNewClient: (parent, { client }, { dataSources }, info) => {
    return dataSources.ClientAPI.addClient(client);
  }
};
