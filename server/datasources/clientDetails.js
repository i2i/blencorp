const { RESTDataSource } = require("apollo-datasource-rest");

class ClientDetailsAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = `${process.env.API_BASE_URL}clientDetails`;
  }

  async getClientDetailsById(id) {
    const data = await this.get(`/${id}`);
    return data;
  }

  async getClientDetails() {
    const data = await this.get("/");
    return data;
  }
}

module.exports = ClientDetailsAPI;
