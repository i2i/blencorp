const Query = require("./resolvers/query");
const Client = require("./resolvers/client");

module.exports = {
  Query,
  Client
};
